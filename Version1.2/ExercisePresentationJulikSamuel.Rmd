---
title: "ExercisePresentation1"
author: "Samuel Julik"
date: "2/13/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Start of data entry

This is my attempt at summarizing a year of weightlifting data.

```{r}
library(readxl)
library(ggplot2)
library(stringr)
library(dplyr)
totalLifts <- read_xlsx("LiftJournalSamJulik.xlsx")
summary(totalLifts)
ggplot(totalLifts, aes(x = ID, y = Weight, color = Exercise)) +
  geom_point()
```

## Create a new data set to personal records and progress

Squats, OH Press, and Bench.

```{r}
colnames(totalLifts)


nrow(totalLifts)

Squats1 <- data.frame( ID = numeric(),
                  Exercise = character(),
                  Weight = numeric(),
                  Repetitions = numeric(),
                  Sets = numeric()
                  )

Squats1 <- totalLifts[totalLifts$Exercise == "Squats",]

#Squats

ggplot(Squats1, aes(x = ID, y = Weight, color = Sets)) +
  geom_point() + ggtitle("Squats")

Press <- data.frame( ID = numeric(),
                     Exercise = character(),
                     Weight = numeric(),
                     Repetitions = numeric(),
                     Sets = numeric()
                     )

Press <- totalLifts[totalLifts$Exercise == "OH Press",]

#OH Press

ggplot(Press, aes(x = ID, y = Weight, color = Sets)) +
  geom_point() + ggtitle("Overhead Press")

Bench <- totalLifts[totalLifts$Exercise == "Bench",]

# Bench

ggplot(Bench, aes(x = ID, y = Weight, color = Sets)) +
  geom_point() + ggtitle("Bench Press")



```



## Endurance data set. Multiplication of Sets, Reps, and Weight.

Sets * Reps * Weight, highest values

```{r}

Totalweight <- totalLifts[totalLifts$Exercise == c("Bench","OH Press", "Deadlift","Row","Squats"),]

ggplot(Totalweight, aes(x = ID, y = as.numeric(Weight)*as.numeric(Repetitions)*as.numeric(Sets), color = Weight)) +
  geom_point() + ggtitle("Reps x Sets x Weight, All Main Stronglifts Exercises")

```



## Day Dataframe of Exercises

Primary Key = Days Spent Training Since 2/4/2020

```{r}

# totalLifts$Exercise

Stronglifts <- data.frame(totalLifts)

UsefulIndex <- c(grep("Bench", totalLifts$Exercise), grep("OH Press", totalLifts$Exercise),
               grep("Deadlift", totalLifts$Exercise), grep("Row", totalLifts$Exercise),
               grep("Squats", totalLifts$Exercise))

# Stronglifts$ID

Stronglifts <- Stronglifts[UsefulIndex,]

BadIndex <- c(grep("Body-Squats", Stronglifts$Exercise), grep("Front-Squats", Stronglifts$Exercise))

Stronglifts <- Stronglifts[-BadIndex,]

Stronglifts <- arrange(Stronglifts,ID,by_group = FALSE)

#  At this point, the Stronglifts dataframe is arranged chronologically again.
# Now to count the days since I started in 2020.

DaysLifting <- data.frame(Key = numeric(),
                          Date = character())

DaysLifting <- DaysLifting[c(1:267),]
DaysLifting$Key <- 1:267

# The day counting is a WIP.



# Working backwards, start by dividing the StrongLifts dataframe by year.

StrongliftsFirst <- Stronglifts[Stronglifts$Year == 2020,]

TemplateOne <- data.frame(ID = numeric(),
                          Exercise = character(),
                          Sets = numeric(),
                          Repetitions = numeric(),
                          Routine = numeric(),
                          Weight = numeric(),
                          EnduranceWeight = numeric(),
                          SumEndurance = numeric())



Counter <- 0
MonthReportTool <- function(DFI,MonthInput){
  IndexByMonth <- c(grep(MonthInput,DFI$Month))
  CurrentMonth <- DFI[IndexByMonth,]
  CopyTemplate <- TemplateOne
  
  i <- 1
  for(i in 1:nrow(CurrentMonth)){
    CopyTemplate[i,1:6] <- CurrentMonth[i,c("ID","Exercise",
                                        "Sets","Repetitions",
                                        "Routine","Weight")]
    CopyTemplate[i,7] <- (as.numeric(CopyTemplate[i,3])
                          * as.numeric(CopyTemplate[i,4])
                          * as.numeric(CopyTemplate[i,6]))
    if(i == 1){
      CopyTemplate[i,8] <- CopyTemplate[i,7]
    }else{
      CopyTemplate[i,8] <- (as.numeric(CopyTemplate[i,7]) 
                           + as.numeric(CopyTemplate[(i-1),8]))
    }
  }
  return(CopyTemplate)
}

MonthReportTool(StrongliftsFirst,"February")
  




```

