---
title: "ExercisePresentation 1.4"
author: "Samuel Julik"
date: "1/2/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Start of data entry

This is my attempt at summarizing a year of weightlifting data.

```{r}
library(readxl)
library(ggplot2)
library(stringr)
library(dplyr)
totalLifts <- read_xlsx("LiftJournalSamJulik.xlsx")
# summary(totalLifts)
ggplot(totalLifts, aes(x = ID, y = Weight, color = Exercise)) +
  geom_point()
```

## Create a new data set to personal records and progress

Squats, OH Press, and Bench.

```{r}
colnames(totalLifts)


nrow(totalLifts)

# Squats1 <- data.frame( ID = numeric(),
#                   Exercise = character(),
#                   Weight = numeric(),
#                   Repetitions = numeric(),
#                   Sets = numeric()
#                   )

Squats1 <- totalLifts[totalLifts$Exercise == "Squats",]

#Squats

ggplot(Squats1, aes(x = ID, y = Weight, color = Sets)) +
  geom_point() + ggtitle("Squats")

# Press <- data.frame( ID = numeric(),
#                      Exercise = character(),
#                      Weight = numeric(),
#                      Repetitions = numeric(),
#                      Sets = numeric()
#                      )

Press <- totalLifts[totalLifts$Exercise == "OH Press",]

#OH Press

ggplot(Press, aes(x = ID, y = Weight, color = Sets)) +
  geom_point() + ggtitle("Overhead Press")

Bench <- totalLifts[totalLifts$Exercise == "Bench",]

# Bench

ggplot(Bench, aes(x = ID, y = Weight, color = Sets)) +
  geom_point() + ggtitle("Bench Press")



```



## Endurance data set. Multiplication of Sets, Reps, and Weight.

Sets * Reps * Weight, highest values

```{r}

Totalweight <- totalLifts[totalLifts$Exercise == c("Bench","OH Press", "Deadlift","Row","Squats"),]

ggplot(Totalweight, aes(x = ID, y = as.numeric(Weight)*as.numeric(Repetitions)*as.numeric(Sets), color = Weight)) +
  geom_point() + ggtitle("Reps x Sets x Weight, All Main Stronglifts Exercises")

```



## Day Dataframe of Exercises

Primary Key = Days Spent Training Since 2/4/2020

The graphs below display the first and second quarters of 2020.


```{r}

# totalLifts$Exercise

Stronglifts <- data.frame(totalLifts)

UsefulIndex <- grep("(Bench)|(OH Press)|(Deadlift)|(Row)|(Squats)", totalLifts$Exercise)

# Stronglifts$ID

Stronglifts <- Stronglifts[UsefulIndex,]

BadIndex <- grep("(Body-Squats)|(Front-Squats)", Stronglifts$Exercise)

Stronglifts <- Stronglifts[-BadIndex,]

Stronglifts <- arrange(Stronglifts,ID,by_group = FALSE)

#  At this point, the Stronglifts dataframe is arranged chronologically again.
#  It may be useful to graph. Oops. I didn't compute Endurance yet.
#  I will graph it further down.

# Now to count the days since I started in 2020.
# 
# DaysLifting <- data.frame(Key = numeric(),
#                           Date = character())
# 
# DaysLifting <- DaysLifting[c(1:267),]
# DaysLifting$Key <- 1:267

# The day counting is a WIP.



# Working backwards, start by dividing the StrongLifts dataframe by year.

StrongliftsFirst <- Stronglifts[Stronglifts$Year == 2020,]

TemplateOne <- data.frame(ID = numeric(),
                          Exercise = character(),
                          Sets = character(),
                          Repetitions = numeric(),
                          Routine = character(),
                          Weight = numeric(),
                          EnduranceWeight = numeric(),
                          SumEndurance = numeric(),
                          QuarterKey = numeric(),
                          TotalDays = numeric(),
                          Category = numeric(),
                          Quarter = numeric(),
                          Year = numeric())

QuarterlyReportDF <- TemplateOne

Q1 <- select(filter(right_join(TemplateOne,Stronglifts),Month==c("January",
                  "February","March"),Year==2020),
             ID,Exercise,Sets,Repetitions,Routine,Weight,
             EnduranceWeight,Category,Quarter,Year)

Q1 <- mutate(Q1, Quarter = 1)

Q1 <- mutate(Q1, EnduranceWeight = as.numeric(Repetitions) * as.numeric(Sets) * 
               as.numeric(Weight))

QuarterlyReportDF <- right_join(QuarterlyReportDF,Q1)

# QuarterlyReportDF

Q2 <- select(filter(right_join(TemplateOne,Stronglifts),Month==c("April",
                  "May","June"),Year==2020),
             ID,Exercise,Sets,Repetitions,Routine,Weight,
             EnduranceWeight,Category,Quarter,Year)

Q2 <- mutate(Q2, Quarter = 2)

Q2 <- mutate(Q2, EnduranceWeight = as.numeric(Repetitions) * as.numeric(Sets) * 
               as.numeric(Weight))

QuarterlyReportDF <- full_join(QuarterlyReportDF,Q2)

Q3 <- select(filter(right_join(TemplateOne,Stronglifts),Month==c("July",
                  "August","September"),Year==2020),
             ID,Exercise,Sets,Repetitions,Routine,Weight,
             EnduranceWeight,Category,Quarter,Year)

Q3 <- mutate(Q3, Quarter = 3)

Q3 <- mutate(Q3, EnduranceWeight = as.numeric(Repetitions) * as.numeric(Sets) * 
               as.numeric(Weight))

QuarterlyReportDF <- full_join(QuarterlyReportDF,Q3)

# I should have used dplyr for the MonthReportTool.

Q4 <- select(filter(right_join(TemplateOne,StrongliftsFirst),Month==c("October","November","December")),ID,Exercise,Sets,Repetitions,Routine,Weight,
             EnduranceWeight,Category,Quarter,Year)

Q4 <- mutate(Q4, Quarter = 4)

Q4 <- mutate(Q4, EnduranceWeight = as.numeric(Repetitions) * as.numeric(Sets) * 
               as.numeric(Weight))

QuarterlyReportDF2020 <- full_join(QuarterlyReportDF,Q4)

# QuarterlyReportDF2020


SquatsRDF2020 <- QuarterlyReportDF2020
 
 i <- 1
 for( i in 1:nrow(SquatsRDF2020)){
   if(is.na(SquatsRDF2020[i,3])){
     SquatsRDF2020[i,3] <- 0
   }
   if(is.na(SquatsRDF2020[i,4])){
     SquatsRDF2020[i,4] <- 0
   }
 }
 
# Need to check whether this above line is necessary.


# SquatsRDF2020

# The ID assignment below is not working properly.
# Q4[1:nrow(Q4),9] <- 1:nrow(Q4)
# The above line assigns a unique ID to each entry of each Quarter.

# Q4

CategoryFunction <- function(DF){
y <- 1

for( y in 1:nrow(DF)){
  if( (as.numeric(DF[y,3]) * as.numeric(DF[y,4])) == 25){
  DF[y,11] <- 4
  }
    if( (as.numeric(DF[y,3])*as.numeric(DF[y,4]) > 3) &
        (as.numeric(DF[y,3])*as.numeric(DF[y,4]) < 25)){
  DF[y,11] <- 3
    }
      if( (as.numeric(DF[y,3])*as.numeric(DF[y,4]) > 0) &
        (as.numeric(DF[y,3])*as.numeric(DF[y,4]) <= 3)){
  DF[y,11] <- 2
      }
    if( as.numeric(DF[y,3])*as.numeric(DF[y,4]) == 0){
  DF[y,11] <- 1
  }
}

return(DF)
}

 SquatsRDF2020 <- CategoryFunction(SquatsRDF2020)

metaDF <- data.frame(ID = numeric(),
                     Quarter = numeric(),
                     Year = numeric(),
                     LinearModelInfo = numeric(),
                     Slope = numeric(),
                     Intercept = numeric(),
                     R = numeric(),
                     CatStat = numeric())

LinearModelBuild <- function(DF,YearV){
  x <- 1
  y <- 1
  NewMetaDF <- metaDF
  for(x in 1:24){
    NewMetaDF[x,1] <- x
    NewMetaDF[x,2] <- ((x - 1) %/% 6) + 1
#   NewMetaDF[x,8] <- 
    
    
    NewMetaDF[x,3] <- YearV
    NewMetaDF[x,4] <- 6 - (6 - x %% 6) %% 6 # Didn't understand how this worked but it did.
    if( x == 1){
    NewMetaDF[x,5] <- lm(Weight~ID,filter(DF,Year==2020,Category==1))[[1]][[2]]
    NewMetaDF[x,6] <- lm(Weight~ID,filter(DF,Year==2020,Category==1))[[1]][[1]]
    }
    if( x == 2){
     NewMetaDF[x,5] <- lm(Weight~ID,filter(DF,Year==YearV,Category==2,
                                           Quarter == 1))[[1]][[2]]
     NewMetaDF[x,6] <- lm(Weight~ID,filter(DF,Year==YearV,Category==2,
                                           Quarter == 1))[[1]][[1]]
      
      # I'm running low on motivation to complete this.
    }
    
    
    # I'll explain why I chose to use 6 linear models.
    # 1-4 are the categories, 5 is the mean of each categorical regression line,
    # and 6 is the overall regression for the whole data set.
  }
    
return(NewMetaDF)
}

 
 LinearModelBuild(SquatsRDF2020,2020)


SquatsRDF2020
select(filter(SquatsRDF2020, Quarter == 1),Category)


```

